#!/bin/bash

launchctl stop com.kevin.wallpaperd
launchctl unload ~/Library/LaunchAgents/com.kevin.wallpaperd.plist

rm ~/bin/wallpaperd
rm ~/Library/LaunchAgents/com.kevin.wallpaperd.plist
