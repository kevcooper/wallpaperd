#!/bin/bash

mkdir -p ~/Library/LaunchAgents
mkdir -p ~/bin
cp wallpaperd ~/bin/.
cp com.kevin.wallpaperd.plist ~/Library/LaunchAgents/. 
sed -i '' "s:/Users/foo:$HOME:g" ~/Library/LaunchAgents/com.kevin.wallpaperd.plist

launchctl load ~/Library/LaunchAgents/com.kevin.wallpaperd.plist
launchctl start com.kevin.wallpaperd
